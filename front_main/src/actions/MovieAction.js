import axios from 'axios';

export function getMovies() {
    return function(dispatch) {
        axios.get('https://cors.io/?https://rss.itunes.apple.com/api/v1/us/movies/top-movies/all/50/explicit.json').then(res => {
            dispatch({
                type: 'GET_MOVIES',
                payload: res.data.feed.results
            })
        }).catch((err) => {
            console.log(err, "err")
        })
    }

}