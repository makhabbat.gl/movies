const initialState = {
    movies: [],
    favors: []
}

export default function auth(state = initialState, action) {

    switch (action.type) {
        case 'GET_MOVIES':
            return {
                ...state,
                movies: action.payload
            }
        default:
            return state;
    }

}