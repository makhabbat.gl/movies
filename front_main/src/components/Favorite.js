import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import * as MovieActions from '../actions/MovieAction'
import sr from './ScrollReveal'

class Favor extends React.Component{

    componentDidMount(){
        const config = {
            duration: 1000,
            delay: 150,
            distance: '500px',
            scale: 1,
            easing: 'ease',
          }
      
        sr.reveal('.movie_container', config)
    }

    render(){
        return(
            <div className="w-90 mr-auto ml-auto text-center">
                <p className="color_white font-size-30 font-weight-700">Favorite movies</p>
                <Link to="/" style={{ textDecoration: 'none' }}><p className="color_ef6d51 font-size-15 font-weight-400 cursor">All movies</p></Link>
                <div className="flex fl-c fl-frw">
                {
                    this.props.favors[0] ? this.props.favors.map((movie, index) => (                        
                       <div className="bg_white movie_container">
                            <a href={movie.url} target="_blank" style={{ textDecoration: 'none' }}>
                                <img src={movie.artworkUrl100} className="mt-5"/>
                                <p className="color_2e2e32 font-size-15 m-5px">{movie.name}</p>
                            </a>
                        </div> 
                    )) : <p className="color_white">You haven't added any favorite movies yet.</p>
                }
                </div>
                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    favors: state.movies.favors
})

function mapDispatchToProps(dispatch) {
    return {
        MovieActions: bindActionCreators(MovieActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favor)