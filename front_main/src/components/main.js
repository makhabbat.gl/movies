import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import Spinner from 'react-spinner-material'
import ReactPaginate from 'react-paginate'
import * as MovieActions from '../actions/MovieAction'
import sr from './ScrollReveal'

class Main extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          currentPage: 1,
          todosPerPage: 15
        };
    }

    componentDidMount(){
        this.props.MovieActions.getMovies()        
    }

    addFavor(name, img, url){
        const movie = {
            name: name,
            artworkUrl100: img,
            url: url
        }
        this.props.favors.push(movie)
    }

    render(){
        const config = {
            duration: 1000,
            delay: 150,
            distance: '500px',
            scale: 1,
            easing: 'ease',
        }
        const { currentPage, todosPerPage } = this.state;
        const todos = this.props.movies;
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);
        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(todos.length / todosPerPage); i++) {
          pageNumbers.push(i);
        }

        const renderPageNumbers = pageNumbers.map(number => {
            return (
                <div>
                    <p key={number} id={number} onClick={(event) => this.setState({currentPage: Number(event.target.id)})} className="color_white cursor">
                    {number}
                    </p>
                </div>            
            );
        });

        return(
            <div className="w-95 mr-auto ml-auto text-center">
                {/* <p className="color_white bg_ef6d51 br-5 mr-auto ml-auto pos-absolute p_10px">Added</p>*/}
                <p className="color_white font-size-30 font-weight-700">TOP 25 - US MOVIES</p>
                <Link to="/favorite" style={{ textDecoration: 'none' }}><p className="color_ef6d51 font-size-15 font-weight-400 cursor">Favorite movies</p></Link>
                <div className="flex fl-c fl-frw">
                {
                    this.props.movies[0] ? currentTodos.map((movie, index)=>(
                        sr.reveal('.movie_container', config),                        
                        <div className="bg_white movie_container">
                            <a href={movie.url} target="_blank" style={{ textDecoration: 'none' }}>
                                <img src={movie.artworkUrl100} className="mt-5"/>
                                <p className="color_2e2e32 font-size-15 m-5px">{movie.name}</p>
                            </a>
                            <p className="color_2e2e32 font-size-15 m-10px cursor favor z-index10" 
                            onClick={()=>this.addFavor(movie.name, movie.artworkUrl100, movie.url)}>+ Add to favorites</p>
                        </div>
                    )) : <div className="mt_60px"><Spinner size={120} spinnerColor={"#fff"} spinnerWidth={2} visible={true}/></div>
                }
                </div> 
                <div className="flex fl-sa w-15 ml-auto mr-auto">
                    {renderPageNumbers}
                </div> 
            </div>
        )
    }
}

const mapStateToProps = state => ({
    movies: state.movies.movies,
    favors: state.movies.favors
})

function mapDispatchToProps(dispatch) {
    return {
        MovieActions: bindActionCreators(MovieActions, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Main)