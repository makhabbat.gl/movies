import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router' // react-router v4
import { ConnectedRouter } from 'connected-react-router'
import '../../assets/styles/index.css'

import createHistory from 'history/createBrowserHistory'
const history = createHistory()

import configureStore from './store/configureStore'
const store = configureStore(true)

import Teacher from './containers/Teacher'
import Main from './components/main'
import axios from 'axios'

render( 
    <Provider store = { store }>
        <ConnectedRouter history = { history } >
            <Switch>
            <Route path='/teacher' component={Teacher}/>
			<Route exact path="/" component={Main}/>		
            </Switch> 
        </ConnectedRouter> 
    </Provider>,
    document.getElementById('root')
)